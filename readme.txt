#docker build -t oc-test-1 .
#docker run -d -p 8000:80 oc-test-1
## oc-test-1
---
apiVersion: v1
kind: Service
metadata:
  name: oc-test-1    
spec:
  selector:                  
    app: oc-test-1 
  ports:
  - port: 5000               
    protocol: HTTP
---
apiVersion: v1
kind: ImageStream
metadata:
  name: oc-test-1-build
---
apiVersion: v1
kind: ImageStream
metadata:
  name: oc-test-1-runtime
---
# oc adm policy add-scc-to-user privileged system:serviceaccount:nordseth:builder
apiVersion: v1
kind: BuildConfig
metadata:
  name: oc-test-1-build
spec:
  source:
    git:
      uri: https://gitlab.com/nordseth/oc-test-1.git
  strategy:
    sourceStrategy:
      from:
        kind: DockerImage
        name: microsoft/aspnetcore-build:2.0
      scripts: https://gitlab.com/nordseth/oc-test-1/raw/master/s2i/build
      env:
      - name: DOTNET_ASSEMBLY_NAME
        value: oc-test-1.dll
  output:
    to:
      kind: ImageStreamTag
      name: oc-test-1-build:latest
---
apiVersion: v1
kind: BuildConfig
metadata:
  name: oc-test-1-runtime
spec:
  source:
    images:
    - from: 
        kind: ImageStreamTag
        name: oc-test-1-build:latest
      paths: 
      - sourcePath: /publish
        destinationDir: "."
  triggers:
  - type: ImageChange
    imageChange: 
      from: 
        kind: ImageStreamTag
        name: oc-test-1-build:latest
  strategy:
    sourceStrategy:
      from:
        kind: DockerImage
        name: microsoft/aspnetcore:2.0
      scripts: https://gitlab.com/nordseth/oc-test-1/raw/master/s2i/runtime
      env:
      - name: DOTNET_ASSEMBLY_NAME
        value: oc-test-1.dll
      - name: ASPNETCORE_URLS
        value: http://+:5000
  output:
    to:
      kind: ImageStreamTag
      name: oc-test-1-runtime:latest