FROM microsoft/aspnetcore-build:2.0

WORKDIR /src
COPY . ./
RUN dotnet publish -c Release -o /app

WORKDIR /app
ENV ASPNETCORE_URLS http://+:5000
EXPOSE 5000
ENTRYPOINT ["dotnet", "oc-test-1.dll"]