﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace oc_test_1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            if (Configuration.GetValue<string>("redis_connection") == null)
            {
                services.AddDistributedMemoryCache();
            }
            else
            {
                var connectionStr = Configuration.GetValue<string>("redis_connection", "localhost");
                var passwd = Configuration.GetValue<string>("redis_password");
                if (passwd != null)
                {
                    connectionStr += $",password={passwd}";
                }

                var redis = StackExchange.Redis.ConnectionMultiplexer.Connect(connectionStr);
                services.AddDataProtection().PersistKeysToRedis(redis);

                services.AddDistributedRedisCache(s =>
                {
                    s.Configuration = connectionStr;
                    s.InstanceName = Configuration.GetValue<string>("redis_instanceName") ?? "oc-test-1";
                });
            }
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvcWithDefaultRoute();
        }
    }
}
