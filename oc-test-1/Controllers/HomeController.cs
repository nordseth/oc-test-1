﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;

namespace oc_test_1.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDistributedCache _cache;
        private readonly IConfiguration _config;

        public HomeController(IDistributedCache cache, IConfiguration config)
        {
            _cache = cache;
            _config = config;
        }

        public IActionResult Index()
        {
            try
            {
                var s = _cache.GetString("test");
                s = s + "1";
                _cache.SetString("test", s);
                ViewBag.test = s;
            }
            catch
            {
                ViewBag.test = "unable to connect to cache!";
            }

            ViewBag.config = _config.AsEnumerable().OrderBy(kv => kv.Key);
            return View();
        }
    }
}